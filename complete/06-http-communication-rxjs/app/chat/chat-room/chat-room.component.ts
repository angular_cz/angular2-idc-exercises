import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChatRoom, Message } from "../../model/chat";

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit {

  @Input() chatRoom: ChatRoom;
  @Output() send = new EventEmitter<Message>();

  constructor() {
  }

  ngOnInit() {
  }

}
