import { Component } from '@angular/core';
import { User } from './model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user: User = {
    name: 'Pavel',
    surname: 'Novák',
    twitterName: '@pavelnovak',

    contact: {
      phone: '530 314 975'
    },

    registrationDate: new Date(2014, 9, 20)
  };

  userSubmitTemplateBased(user: User) {
    console.log('Submited from templated based form: ', user);
    this.user = user;
  }

  userSubmitReactive(user: User) {
    console.log('Submitted from reactive form: ', user);
    this.user = user;
  }


}
