import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Order } from '../model/order';

@Component({
  selector: 'app-orders-list',
  templateUrl: 'orders-list.component.html'
})
export class OrdersListComponent {

  @Input() orders: Order[];
  @Input() selectedOrder: Order;

  // TODO 3.2 - vytvořte výstupní událost selectedOrderChange

  select(order: Order) {

    // TODO 3.3 - odešlete událost selectedOrderChange
  }
}
