import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormMetadataViewerComponent } from './form-metadata-viewer/form-metadata-viewer.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    FormMetadataViewerComponent,
    UserProfileComponent
  ],
  exports: [
    FormMetadataViewerComponent,
    UserProfileComponent
  ]
})
export class SharedModule {
}
